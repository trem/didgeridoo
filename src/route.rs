
use rocket::{
	response::stream::ByteStream,
	response::content::RawHtml,
	http::ContentType,
	State,
};

use std::fs;

use crate::{index::SongIndex, Config};


const INDEX_HTML: &str = include_str!("../static/index.html");
const FAVICON: &[u8] = include_bytes!("../static/favicon.ico");


#[get("/")]
pub fn index() -> RawHtml<&'static str> {
	RawHtml(INDEX_HTML)
}

#[get("/favicon.ico")]
pub fn favicon() -> (ContentType, &'static [u8]) {
	(ContentType::PNG, FAVICON)
}


#[get("/stream")]
pub fn stream(song_index: &State<SongIndex>, config: &State<Config>) -> ByteStream![Vec<u8>] {
	
	let songs = song_index.songs.clone();
	let audio_chunk_bytes = config.audio_chunk_bytes;
	
	ByteStream! {
		loop {
			let position = fastrand::usize(..songs.len());
			let path = &songs[position];
			debug!("Reading file: {}", path.display());
			let bytes = fs::read(path).expect(&format!("Failed to read file: {}", path.display()));
			
			for chunk in bytes.chunks(audio_chunk_bytes) {
				yield chunk.to_owned()
			}
		}
	}
}
