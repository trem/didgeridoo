#[macro_use] extern crate rocket;
use rocket::fairing::AdHoc;

use clap::Parser;

use std::path::PathBuf;

mod config;
pub use config::Config;
mod route;
mod index;


#[derive(Parser)]
#[clap(about, version)]
pub struct Args {
	///IP address or DNS name at which the server should listen [default: 0.0.0.0]
	#[clap(short, long)]
	address: Option<String>,
	
	///Port on which the server should listen [default: 6541]
	#[clap(short, long)]
	port: Option<u16>,
	
	///Directory with music files to stream
	#[clap(default_value=".")]
	directory: PathBuf,
}


#[launch]
fn rocket() -> _ {
	let args = Args::parse();
	let base_dir = args.directory.clone();
	
	rocket::custom(Config::load_and_merge_with(args))
		.attach(AdHoc::config::<Config>())
		.attach(index::SongIndexFairing::new(base_dir))
		.mount("/", routes![route::index, route::favicon, route::stream])
}
