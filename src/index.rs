use rocket::{
	Build,
	Rocket,
	fairing::{self, Fairing, Info, Kind},
};

use std::{
	fs,
	path::Path,
	path::PathBuf,
};


pub struct SongIndex {
	pub songs: Vec<Box<Path>>,
}


pub struct SongIndexFairing {
	base_dir: PathBuf,
}
impl SongIndexFairing {
	pub fn new(base_dir: PathBuf) -> Self {
		Self { base_dir }
	}
}

#[rocket::async_trait]
impl Fairing for SongIndexFairing {
	
	fn info(&self) -> Info {
		Info {
			name: "Song Index",
			kind: Kind::Ignite
		}
	}
	
	async fn on_ignite(&self, rocket: Rocket<Build>) -> fairing::Result {
		
		let base_dir = self.base_dir.clone()
			.canonicalize().unwrap_or_else(|_| panic!("Failed to canonicalize path \"{}\"!", self.base_dir.clone().display()));
		
		let base_dir_string = base_dir.display();
		
		
		info!("Indexing songs under: {}", base_dir_string);
		
		fn index_files(dir: &Path) -> Result<Vec<Box<Path>>, std::io::Error> {
			
			fs::read_dir(dir)?.fold(Ok(vec![]), |acc, entry| {
				let path = entry?.path();
				let metadata = fs::metadata(&path)?;
				
				if metadata.is_file() {
					let path_str = path.display();
					
					let file_type = infer::get_from_path(&path)
						.unwrap_or_else(|_| panic!("Failed to determine type of file: {path_str}"));
					
					if let Some(file_type) = file_type {
						if file_type.matcher_type() == infer::MatcherType::Audio {
							debug!("Indexed song: {path_str}");
							acc.map(|mut acc| { acc.push(path.into_boxed_path()); acc })
						}
						else {
							debug!("Skipping non-audio file: {path_str}");
							acc
						}
					} else {
						info!("Skipping file with unknown file type: {path_str}");
						acc
					}
				}
				else {
					let mut children = index_files(&path)?;
					acc.map(|mut acc| { acc.append(&mut children); acc })
				}
			})
		}
		
		let songs = index_files(&base_dir).expect("Failed to index songs.");
		if songs.is_empty() {
			warn!("Could not find any songs in \"{}\"! Start Didgeridoo in a folder with music files or specify the path to it.", base_dir_string);
			Err(rocket)
		}
		else {
			info!("Indexed {} songs in \"{}\".", songs.len(), base_dir_string);
			Ok(rocket.manage(SongIndex { songs }))
		}
	}
}
