use rocket::{
    figment::{Figment, providers::{Format, Toml, Serialized, Env}},
    serde::{Serialize, Deserialize},
};

use crate::Args;



#[derive(Debug, Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct Config {
    pub address: String,
    pub port: u16,
    ///Number of bytes to be sent at once over the audio stream
    pub audio_chunk_bytes: usize,
}
impl Default for Config {
    fn default() -> Config {
        Config {
            address: "0.0.0.0".to_string(),
            port: 6541,
            audio_chunk_bytes: 1024,
        }
    }
}
impl Config {
    pub fn load_and_merge_with(args: Args) -> Figment {
        
        let mut config = Figment::from(rocket::Config::default())
            .merge(("ident", "Didgeridoo"))
            .merge(Serialized::defaults(Config::default()))
            .merge(Toml::file("didgeridoo.toml").profile("default"))
            .merge(Env::prefixed("DIDGERIDOO_").global());
        
        if let Some(address) = args.address {
            config = config.merge(("address", address));
        };
        if let Some(port) = args.port {
            config = config.merge(("port", port));
        };
        
        config
    }
}
