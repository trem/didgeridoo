# Didgeridoo

Didgeridoo serves a webpage which can play back an endless stream of your music files.

* Streams files in a given directory and any sub-directories.
* Songs are played in random order.
* There's no "Previous" or "Next" button.
* The served webpage is JavaScript-free.


## How To Run

If you've downloaded a binary release or already compiled Didgeridoo, you can launch it like so:

```
./didgeridoo ~/Music
```

Once the server has started, you should be able to reach the webpage under `http://localhost:6541`.  
If you host it on a separate machine, replace "localhost" with the IP address or DNS name of that machine, and make sure the port 6541 is reachable in the firewall configuration of the server machine.


## Configuration

You can pass basic configuration parameters via command-line arguments. Run `didgeridoo --help` to learn more.

Didgeridoo uses [Rocket](https://rocket.rs) under the hood and as such supports the same configuration parameters.


### Configuration File

You can pass configuration parameters via a `didgeridoo.toml` file, which you can simply place next to the `didgeridoo` executable.  
Here is an example file with all configuration parameters specified:

```toml
address = "127.0.0.1"
port = 6541
workers = 16
keep_alive = 5
ident = "Rocket"
log_level = "normal"
temp_dir = "/tmp"
cli_colors = true
## NOTE: Don't (!) use this key! Generate your own!
secret_key = "hPRYyVRiMyxpw5sBB1XeCMN1kFsDCqKvBi2QJxBVHQk="

[limits]
forms = "64 kB"
json = "1 MiB"
msgpack = "2 MiB"
"file/jpg" = "5 MiB"

[tls]
certs = "path/to/cert-chain.pem"
key = "path/to/key.pem"

[shutdown]
ctrlc = true
signals = ["term", "hup"]
grace = 5
mercy = 5
```

You only need to set the options that you actually want to customize.

These are the configuration parameters that Rocket uses, so you can reference that documentation for an explanation of the individual parameters: https://rocket.rs/v0.5-rc/guide/configuration/#rockettoml  
It does not use Rocket's profile feature, though, which is why "[default]" and similar headers are omitted.


### Environment Variables

You can also customize Didgeridoo's behavior via environment variables.  
Again, you can reference Rocket's documentation: https://rocket.rs/v0.5-rc/guide/configuration/#environment-variables  
You just have to replace the "`ROCKET_`" prefix with "`DIDGERIDOO_`".


## Compiling From Source

To compile Didgeridoo from source, make sure you have a working Rust toolchain and then run:

```
cargo build --release
```

You can then find the executable under `target/release/didgeridoo`.


## FAQ

### How does Didgeridoo compare to [Icecast](https://icecast.org)?

Icecast is often recommended for streaming your music library through a web browser.  
However, that is not what Icecast was primarily built for. You actually need to configure a plugin to have it output an HTTP stream.  

Didgeridoo limits itself to just the HTTP stream (with an accompanying webpage) and as such can reduce the complexity of the user setup, as well as the implementation.

